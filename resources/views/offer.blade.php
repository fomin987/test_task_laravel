@extends('layouts.main')
@section('content')
@foreach($offers as $offer)
    <a href='{{$offer->url}}'>{{$offer->name}}</a><br>
@endforeach
{{$offers->links()}}
<a class="btn btn-success" href="/export" role="button">Export</a>
@endsection