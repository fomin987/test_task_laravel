@foreach ($menus as $menu)
  <div class='nav-item'>
    <a href="#" class="nav-link">
      <i class="nav-icon fas fa-copy"></i>
      <p>
        {{ $menu->name }}
        <i class="fas fa-angle-left right"></i>
      </p>
    </a>
    <div class="nav nav-treeview">
      @foreach ($menu->childrenMenus as $childMenu)
          @include('include.child_menu', ['child_menu' => $childMenu, 'menus' => $menus])
      @endforeach
    </div>
  </div>
@endforeach