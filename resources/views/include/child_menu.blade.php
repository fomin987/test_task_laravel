<div class='nav-item'>    
    @if ($child_menu->menus->count() > 1)
        <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
            {{ $child_menu->name }}
            <i class="fas fa-angle-left right"></i>
            </p>
        </a>

        <div class="nav nav-treeview">
            @foreach ($child_menu->menus as $childMenu)
                @include('include.child_menu', ['child_menu' => $childMenu])
            @endforeach
        </div>
    @else
        <a href='/{{$child_menu->name}}' class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
            {{ $child_menu->name }}
            </p>
        </a>
    @endif
</div>