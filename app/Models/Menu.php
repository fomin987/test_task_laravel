<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $guarded = false;

    public function menus()
    {
        return $this->hasMany(Menu::class, 'parent_id', 'id');
    }

    public function childrenMenus()
    {
        return $this->menus()->with('childrenMenus');
    }
}
