<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Menu;
use App\Models\Offer;

class MenuController extends Controller
{
    public function index()
{
    $menus = Menu::whereNull('parent_id')->with('childrenMenus')->get();
    return view('layouts.main', ['menus' => $menus]);
}
}
