<?php

namespace App\Http\Controllers;

use App\Models\Offer;
use App\Models\Menu;
use App\Exports\OfferExport;
use Maatwebsite\Excel\Facades\Excel;

class OfferController extends Controller
{
    public function index($name){
        $offers = Offer::where('sub_sub_category', $name)->paginate();
        $menus = Menu::whereNull('parent_id')->with('childrenMenus')->get();
        return view('offer', ['offers' => $offers, 'menus' => $menus]);
    }

    public function export(){
        return Excel::download(new OfferExport, 'offers.xlsx');
    }
}
