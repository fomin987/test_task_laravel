<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->string('id');
            
            $table->boolean('is_available');
            $table->string('url');
            $table->string('price');
            $table->integer('old_price')->nullable();
            $table->string('currency_id');
            $table->string('category');
            $table->string('sub_category')->nullable();
            $table->string('sub_sub_category');
            $table->string('picture');
            $table->string('name');
            $table->string('vendor');

            $table->index('id', 'id_idx');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('offers');
    }
};
