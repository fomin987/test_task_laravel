<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Menu;

class OfferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $xmlDataString = file_get_contents(public_path('export_Ngq.xml'));
        $shop = simplexml_load_string($xmlDataString)->shop;
        $menus = Menu::all();
        foreach($shop->offers[0] as $offer){
            $sub_sub_category = $menus->find((int)$offer->categoryId);
            $sub_category = $menus->find($sub_sub_category->parent_id);
            $category = $menus->find($sub_category->parent_id);
            
            $sub_sub_category = $sub_sub_category->name;
            $sub_category = $sub_category->name;
            if($category === null){
                $category = $sub_category;
                $sub_category = null;
            }
            else{
                $category = $category->name;
            }

            DB::table('offers')->insert([
                'id' => $offer->attributes()['id'],
                'is_available' => (string)$offer->attributes()['available'] === 'true', 
                'url' => $offer->url,
                'price' => $offer->price,
                'old_price' => $offer->oldprice ? $offer->oldprice : null,
                'currency_id' => $offer->currencyId,
                'sub_sub_category' => $sub_sub_category,
                'sub_category'=> $sub_category,
                'category' => $category,
                'picture' => $offer->picture,
                'name' => $offer->name,
                'vendor' => $offer->vendor
            
            ]);
        }
    }
}
