<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $xmlDataString = file_get_contents(public_path('export_Ngq.xml'));
        $shop = simplexml_load_string($xmlDataString)->shop;
        
        foreach($shop->categories[0] as $category){
            DB::table('menus')->insert([
                'id' => (int)$category->attributes()->id,
                'name' => (string)$category[0],
                'parent_id' => (int)$category->attributes()->parentId ? $category->attributes()->parentId : null
            ]);
        }
            
    } 
}
